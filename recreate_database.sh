#!/usr/bin/env bash
sudo docker-compose exec php-fpm bin/console doctrine:schema:drop --force --env=$1 --em=default
sudo docker-compose exec php-fpm bin/console doctrine:schema:drop --force --env=$1 --em=planet_test1
sudo docker-compose exec php-fpm bin/console doctrine:schema:drop --force --env=$1 --em=planet_test2
sudo docker-compose exec php-fpm bin/console doctrine:schema:create --env=$1 --em=default
sudo docker-compose exec php-fpm bin/console doctrine:schema:create --env=$1 --em=planet_test1
sudo docker-compose exec php-fpm bin/console doctrine:schema:create --env=$1 --em=planet_test2
sudo docker-compose exec php-fpm bin/console doctrine:fixtures:load --env=$1 --em=default -qvv
