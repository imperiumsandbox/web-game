FROM havranro/webgame:1.3

COPY ./ /var/www/mysite/

WORKDIR /var/www/mysite/


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN cd /var/www/mysite && \
    composer install --no-interaction

RUN chmod -R 777 /var/www/mysite/var

