<?php
namespace AppBundle\Builder;

use AppBundle\Descriptor\TimeTransformator;
use AppBundle\Entity\Human;
use AppBundle\Entity\Human\Title;
use AppBundle\LoggedUserSettings;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Concept\ColonizationShip;
use PlanetBundle\Entity as PlanetEntity;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Human\Event;

class EventBuilder
{
    /** @var ObjectManager */
    private $generalEntityManager;
    /** @var ObjectManager */
    private $planetEntityManager;
    /** @var DynamicPlanetConnector */
    private $dynamicPlanetConnector;

    /**
     * EventBuilder constructor.
     * @param ObjectManager $generalEntityManager
     * @param ObjectManager $planetEntityManager
     * @param DynamicPlanetConnector $dynamicPlanetConnector
     */
    public function __construct(ObjectManager $generalEntityManager, ObjectManager $planetEntityManager, DynamicPlanetConnector $dynamicPlanetConnector)
    {
        $this->generalEntityManager = $generalEntityManager;
        $this->planetEntityManager = $planetEntityManager;
        $this->dynamicPlanetConnector = $dynamicPlanetConnector;
    }


    /**
     * @param Human $newOwner
     * @param Title $inheritation
     * @return Event\Inheritance
     */
    public function titleInheritance(Human $newOwner, Title $inheritation) {
        $inheritationEvent = new Event\Inheritance($newOwner, $inheritation);
        $inheritationEvent->setPlanet(DynamicPlanetConnector::getPlanet());
        $inheritationEvent->setPlanetPhase(DynamicPlanetConnector::getPlanet()->getLastPhaseUpdate());
        $this->generalEntityManager->persist($inheritationEvent);
        return $inheritationEvent;
    }
    /**
     * @param PlanetEntity\HumanAggregator $newOwner
     * @param Title $inheritation
     * @return Event\TitleLost
     */
    public function titleLost(PlanetEntity\HumanAggregator $newOwner, Title $inheritation) {
        $inheritationEvent = new Event\TitleLost($newOwner->getGlobalHuman(), $inheritation);
        $inheritationEvent->setPlanet(DynamicPlanetConnector::getPlanet());
        $inheritationEvent->setPlanetPhase(DynamicPlanetConnector::getPlanet()->getLastPhaseUpdate());
        $this->generalEntityManager->persist($inheritationEvent);
        return $inheritationEvent;
    }

    /**
     * @param PlanetEntity\HumanAggregator $died
     * @return Event\HumanDied
     */
    public function death(PlanetEntity\HumanAggregator $died) {
        $deathEvent = new Event\HumanDied($died->getGlobalHuman());
        $deathEvent->setPlanet(DynamicPlanetConnector::getPlanet());
        $deathEvent->setPlanetPhase(DynamicPlanetConnector::getPlanet()->getLastPhaseUpdate());
        $this->generalEntityManager->persist($deathEvent);
        return $deathEvent;
    }

    /**
     * @param Human $born
     * @return Event\HumanBorn
     */
    public function birth(PlanetEntity\HumanAggregator $born) {
        $bitrhEvent = new Event\HumanBorn($born->getGlobalHuman());
        $bitrhEvent->setPlanet(DynamicPlanetConnector::getPlanet());
        $bitrhEvent->setPlanetPhase(DynamicPlanetConnector::getPlanet()->getLastPhaseUpdate());
        $this->generalEntityManager->persist($bitrhEvent);
        return $bitrhEvent;
    }

    /**
     * @param PlanetEntity\HumanAggregator $parent
     * @param PlanetEntity\HumanAggregator $born
     * @return Event\HumanHasOffspring
     */
    public function newOffspring(PlanetEntity\HumanAggregator $parent, PlanetEntity\HumanAggregator $born) {
        $bitrhEvent = new Event\HumanHasOffspring($parent, $born);
        $bitrhEvent->setPlanet(DynamicPlanetConnector::getPlanet());
        $bitrhEvent->setPlanetPhase(DynamicPlanetConnector::getPlanet()->getLastPhaseUpdate());
        $this->generalEntityManager->persist($bitrhEvent);
        return $bitrhEvent;
    }

    /**
     * @param PlanetEntity\Settlement $settlement
     * @param PlanetEntity\Human $founder
     * @return PlanetEntity\Event\SettlementFoundEvent
     */
    public function settlementFounded(PlanetEntity\Settlement $settlement, PlanetEntity\Human $founder) {
        $settlementEvent = new PlanetEntity\Event\SettlementFoundEvent($founder, $settlement);
        $this->planetEntityManager->persist($settlementEvent);
        return $settlementEvent;
    }

    public function create($name, $human, $data) {

    }

}