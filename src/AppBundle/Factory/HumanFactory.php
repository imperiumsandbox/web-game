<?php
namespace AppBundle\Factory;

use AppBundle\Descriptor\TimeTransformator;
use AppBundle\Entity\Human;
use AppBundle\Entity\SolarSystem\Planet;
use AppBundle\Entity\Soul;
use Doctrine\Common\Persistence\ObjectManager;

class HumanFactory
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;

    /**
     * GamerFactory constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Planet $planet
     * @param string $name
     * @return Human
     */
    public function create(Planet $planet, $name) {
        $human = new Human();
        $human->setName($name);
        $human->setPlanet($planet);
        $human->setBornPlanet($planet);
        $human->setBornPhase(TimeTransformator::timestampToPhase($planet, time()));
        $human->setCode(md5(time().$name));
        $this->manager->persist($human);
        return $human;
    }
}