<?php
namespace AppBundle\Factory;

use AppBundle\Entity\Gamer;
use AppBundle\Entity\Soul;
use AppBundle\EnumAlignmentType;
use Doctrine\Common\Persistence\ObjectManager;

class SoulFactory
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;

    /**
     * GamerFactory constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Gamer $gamer
     * @param string $name
     * @param string $alignment enum EnumAlignmentType
     * @param array $preferences
     * @return Soul
     */
    public function create(Gamer $gamer, $name, $alignment, $preferences = []) {
        $soul = new Soul();
        $soul->setGamer($gamer);
        $soul->setName($name);
        $soul->setAlignment($alignment);
        foreach ($preferences as $preference) {
            $soul->addPreference($preference);
        }
        $this->manager->persist($soul);
        return $soul;
    }
}