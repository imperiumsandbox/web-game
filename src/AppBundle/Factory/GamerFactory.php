<?php
namespace AppBundle\Factory;

use AppBundle\Entity\Gamer;
use Doctrine\Common\Persistence\ObjectManager;

class GamerFactory
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;

    /**
     * GamerFactory constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $login
     * @return Gamer
     */
    public function createTestAccount($login) {
        $gamer = new Gamer();
        $gamer->setLogin($login);
        $gamer->setPassword($login);
        $this->manager->persist($gamer);
        return $gamer;
    }
}