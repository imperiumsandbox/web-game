<?php

namespace AppBundle\Entity\Human\Event;

use AppBundle\Entity\Human;
use AppBundle\Entity\PlanetAndPhaseTrait;
use AppBundle\Entity\SolarSystem\Planet;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ORM\Table(name="event_title_loses")
 * @ORM\Entity()
 */
class TitleLost extends Human\Event
{
    /**
     * @var Human\Title
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Human\Title")
     * @ORM\JoinColumn(name="title_id", referencedColumnName="id", nullable=false)
     */
    private $title;

    public function __construct(Human $human, Human\Title $title)
    {
        parent::__construct($human);
        $this->title = $title;
    }


    /**
     * @return Human\Title
     */
    public function getTitle()
    {
        return $this->title;
    }
}

