<?php

namespace AppBundle\Entity\Human\Event;

use AppBundle\Entity\Human;
use AppBundle\Entity\PlanetAndPhaseTrait;
use AppBundle\Entity\SolarSystem\Planet;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ORM\Table(name="event_deaths")
 * @ORM\Entity()
 */
class HumanDied extends Human\Event
{
}

