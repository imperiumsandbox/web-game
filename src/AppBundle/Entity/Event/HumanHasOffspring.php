<?php

namespace AppBundle\Entity\Human\Event;

use AppBundle\Entity\Human;
use AppBundle\Entity\PlanetAndPhaseTrait;
use AppBundle\Entity\SolarSystem\Planet;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ORM\Table(name="event_offsprings")
 * @ORM\Entity()
 */
class HumanHasOffspring extends Human\Event
{
    /**
     * @var Human
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Human")
     * @ORM\JoinColumn(name="human_offspring_id", referencedColumnName="id", nullable=false)
     */
    private $offspring;

    public function __construct(Human $parent, Human $offspring)
    {
        parent::__construct($parent);
        $this->offspring = $offspring;
    }

    /**
     * @return Human
     */
    public function getOffspring()
    {
        return $this->offspring;
    }
}

