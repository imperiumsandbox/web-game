<?php
namespace PlanetBundle\Factory;

use AppBundle\Descriptor\TimeTransformator;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity\Human;

class HumanFactory
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;

    /**
     * GamerFactory constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param $code
     * @return Human
     */
    public function create($code) {
        $human = new Human($code);
        $this->manager->persist($human);
        return $human;
    }
}