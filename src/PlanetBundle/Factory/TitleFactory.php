<?php

namespace PlanetBundle\Factory;

use AppBundle\Entity\Human;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use AppBundle\Repository\HumanRepository;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity as PlanetEntity;
use PlanetBundle\Entity\SettlementTitle;
use PlanetBundle\Repository\TitleRepository;

class TitleFactory
{
    /** @var ObjectManager */
    private $generalEntityManager;

    /** @var ObjectManager */
    private $planetEntityManager;

    /** @var TitleRepository */
    private $titleRepository;

    /**
     * TitleFactory constructor.
     * @param ObjectManager $generalEntityManager
     * @param ObjectManager $planetEntityManager
     * @param TitleRepository $titleRepository
     */
    public function __construct(ObjectManager $generalEntityManager, ObjectManager $planetEntityManager, TitleRepository $titleRepository)
    {
        $this->generalEntityManager = $generalEntityManager;
        $this->planetEntityManager = $planetEntityManager;
        $this->titleRepository = $titleRepository;
    }

    /**
     * @param PlanetEntity\Settlement $settlement
     * @return PlanetEntity\SettlementTitle
     */
    public function createIfNotExists(PlanetEntity\Settlement $settlement) {
        $protectorTitle = $this->titleRepository->findOneBy([
            'settlement' => $settlement,
        ]);
        if ($protectorTitle != null) {
            return $protectorTitle;
        }
        return $this->create($settlement);
    }

    /**
     * @param PlanetEntity\Settlement $settlement
     * @return PlanetEntity\SettlementTitle
     */
    public function create(PlanetEntity\Settlement $settlement) {
        $protectorTitle = new SettlementTitle();
        $protectorTitle->setName($settlement->getName());
        $protectorTitle->setTransferSettings([
            'inheritance' => 'primogeniture',
        ]);
        $protectorTitle->setSettlement($settlement);
        $this->planetEntityManager->persist($protectorTitle);
        return $protectorTitle;
    }

}