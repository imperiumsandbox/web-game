<?php
namespace PlanetBundle\Factory;

use AppBundle\Entity\SolarSystem\Planet;
use AppBundle\Entity\Soul;
use AppBundle\Factory\HumanFactory as GlobalHumanFactory;
use PlanetBundle\Factory\HumanFactory as LocalHumanFactory;
use PlanetBundle\Entity\HumanAggregator;

class HumanAggregatorFactory
{
    /** @var GlobalHumanFactory */
    private $globalHumanFactory;
    /** @var LocalHumanFactory */
    private $localHumanFactory;

    /**
     * HumanAggregatorFactory constructor.
     * @param GlobalHumanFactory $globalHumanFactory
     * @param HumanFactory $localHumanFactory
     */
    public function __construct(GlobalHumanFactory $globalHumanFactory, HumanFactory $localHumanFactory)
    {
        $this->globalHumanFactory = $globalHumanFactory;
        $this->localHumanFactory = $localHumanFactory;
    }

    /**
     * @param Planet $planet
     * @param string $name
     * @return HumanAggregator
     */
    public function create(Planet $planet, $name) {
        $globalHuman = $this->globalHumanFactory->create($planet, $name);
        $localHuman = $this->localHumanFactory->create($globalHuman->getCode());
        return new HumanAggregator($globalHuman, $localHuman);
    }
}