<?php
namespace PlanetBundle\Factory;

use AppBundle\Descriptor\TimeTransformator;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity\Human;
use PlanetBundle\Entity\Peak;
use PlanetBundle\Entity\PeakDeposit;
use PlanetBundle\Entity\Region;
use PlanetBundle\Entity\Resource\StandardizedDeposit;
use PlanetBundle\Entity\Settlement;

class SettlementFactory
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;

    /**
     * GamerFactory constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Peak $administrativeCenter
     * @param StandardizedDeposit $colonizationShipSupplies
     * @return Settlement
     */
    public function createColony(Peak $administrativeCenter, StandardizedDeposit $colonizationShipSupplies) {
        if ($administrativeCenter->getDeposit() == null) {
            $administrativeCenter->setDeposit(new PeakDeposit($administrativeCenter));
        }

        $settlement = new Settlement($administrativeCenter);
        $settlement->setType('village');
        $settlement->setRegions($this->manager->getRepository(Region::class)->findPeakSurrounding($administrativeCenter));

        foreach ($colonizationShipSupplies as $resourceDescriptor) {
            $resourceCopy = clone $resourceDescriptor;
            $resourceCopy->setDeposit($settlement->getAdministrativeCenter()->getDeposit());
            $settlement->getAdministrativeCenter()->getDeposit()->addResourceDescriptors($resourceCopy);
            $this->manager->persist($resourceCopy);
            $this->manager->persist($settlement->getAdministrativeCenter()->getDeposit());
        }

        $this->manager->persist($settlement);
        return $settlement;
    }
}