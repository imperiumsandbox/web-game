<?php
namespace PlanetBundle\Repository;

use AppBundle\Entity;
use PlanetBundle\Entity as PlanetEntity;

class HumanRepository extends \Doctrine\ORM\EntityRepository
{

	public function findByAvailableChildren()
	{
		return $this->getEntityManager()
			->createQuery(
				'SELECT h FROM PlanetBundle:Human h WHERE h.globalHumanId IS NULL ORDER BY h.id ASC'
			)
			->getResult();
	}

	public function findAllIncarnated()
	{
		return $this->getEntityManager()
			->createQuery(
				'SELECT h FROM PlanetBundle:Human h WHERE h.globalHumanCode IS NOT NULL ORDER BY h.id ASC'
			)
			->getResult();
	}

    /**
     * @param Entity\Human $globalHuman
     * @return PlanetEntity\HumanAggregator
     */
    public function getAggregator(Entity\Human $globalHuman)
    {
        $localHuman = $this->findOneBy(['globalHumanCode' => $globalHuman->getCode()]);
        return new PlanetEntity\HumanAggregator($globalHuman, $localHuman);
    }
}