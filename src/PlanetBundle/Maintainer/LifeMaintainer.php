<?php
namespace PlanetBundle\Maintainer;

use AppBundle\Entity\Human;
use AppBundle\Builder\EventBuilder;
use PlanetBundle\Builder\HumanBuilder;
use PlanetBundle\Entity as PlanetEntity;

class LifeMaintainer
{
    /** @var HumanBuilder */
    private $humanBuilder;

    /** @var EventBuilder */
    private $eventBuilder;

    /**
     * LifeMaintainer constructor.
     * @param HumanBuilder $humanBuilder
     * @param EventBuilder $eventBuilder
     */
    public function __construct(HumanBuilder $humanBuilder, EventBuilder $eventBuilder)
    {
        $this->humanBuilder = $humanBuilder;
        $this->eventBuilder = $eventBuilder;
    }

    /**
     * @param Human $human
     * @return int promile
     */
    public function getDeathByAgeProbability(Human $human) {

        if ($human->getAge() <= 40) {
            return 0;
        }
        return ($human->getAge() - 40)/10;
    }

    /**
     * @param PlanetEntity\HumanAggregator $human
     */
    public function kill(PlanetEntity\HumanAggregator $human) {
        $human->setDeathTime(time());

        $this->eventBuilder->death($human);

        $this->inheritTitles($human);

        $human->setTitle(null);
        $human->setTitles([]);
    }

    private function inheritTitles(PlanetEntity\HumanAggregator $human)
    {
        foreach ($human->getTitles() as $title) {
            $this->inheritTitle($title);
        }
    }

    private function inheritTitle(PlanetEntity\Title $title)
    {
        $this->eventBuilder->titleLost($title->getHumanHolder(), $title);
        $heir = $title->getHeir();
        if ($heir != null) {
            $title->setHumanHolder($heir);
            $heir->addTitle($title);
            if ($heir->getTitle() == null) {
                $heir->setTitle($title);
            }

            $this->eventBuilder->titleInheritance($heir, $title);
        } else {
            $title->setHumanHolder(null);
            // TODO: asi povznest nahodneho lowborna do slechtickeho titulu => vyrobit noveho humana
        }
    }

    /**
     * @param PlanetEntity\HumanAggregator $mother
     * @param PlanetEntity\HumanAggregator|null $father
     * @return PlanetEntity\HumanAggregator
     * @throws \Exception
     */
    public function makeOffspring(PlanetEntity\HumanAggregator $mother, PlanetEntity\HumanAggregator $father = null) {
        $offspring = $this->humanBuilder->create($mother, $father);

        $offspring->setName($mother->getName(). ' '.random_int(0, 20));

        $this->eventBuilder->birth($offspring);
        $this->eventBuilder->newOffspring($mother, $offspring);
        if ($father) {
            $this->eventBuilder->newOffspring($father, $offspring);
        }

        return $offspring;
    }
}