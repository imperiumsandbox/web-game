<?php
namespace PlanetBundle\Fixture;

use AppBundle\Fixture\PlanetMapFixture;
use AppBundle\Fixture\PlanetsFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * existuje jen proto aby mohly existovat souhrnne zavislosti pro vsechny fixtures v namespace /Players/
 */
class PersonalFixture extends Fixture implements DependentFixtureInterface
{
	public function load(ObjectManager $generalManager)
	{
        echo __CLASS__."\n";
	}

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            PlanetsFixture::class,
            PlanetMapFixture::class,
            StandardColonizationShipFixture::class,
        ];
    }

}