<?php
namespace PlanetBundle\Fixture\Players;

use AppBundle\Descriptor\TimeTransformator;
use AppBundle\EnumAlignmentType;
use AppBundle\Factory\GamerFactory;
use AppBundle\Factory\SoulFactory;
use AppBundle\Fixture\PlanetMapFixture;
use AppBundle\Fixture\PlanetsFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity as PlanetEntity;
use AppBundle\Entity as GlobalEntity;
use PlanetBundle\Factory\HumanAggregatorFactory;
use PlanetBundle\Fixture\PersonalFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BraseFixture extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    /**
     * The dependency injection container.
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ObjectManager $generalManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
	public function load(ObjectManager $generalManager)
	{
        echo __CLASS__."\n";
        $gamerFactory = new GamerFactory($generalManager);
        $soulFactory = new SoulFactory($generalManager);

		$brase = $gamerFactory->createTestAccount('brase');
        $generalManager->flush();

        $soul = $soulFactory->create($brase, 'Brase the Great',EnumAlignmentType::LAWFUL_GOOD);
        $generalManager->flush();
	}

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            PersonalFixture::class,
        ];
    }

}