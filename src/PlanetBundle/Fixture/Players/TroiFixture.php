<?php
namespace PlanetBundle\Fixture\Players;

use AppBundle\Descriptor\TimeTransformator;
use AppBundle\EnumAlignmentType;
use AppBundle\Factory\GamerFactory;
use AppBundle\Factory\SoulFactory;
use AppBundle\Fixture\PlanetMapFixture;
use AppBundle\Fixture\PlanetsFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity as PlanetEntity;
use AppBundle\Entity as GlobalEntity;
use PlanetBundle\Factory\HumanAggregatorFactory;
use PlanetBundle\Fixture\PersonalFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TroiFixture extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    /**
     * The dependency injection container.
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ObjectManager $generalManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
	public function load(ObjectManager $generalManager)
	{
        echo __CLASS__."\n";
        $gamerFactory = new GamerFactory($generalManager);
        $soulFactory = new SoulFactory($generalManager);

		$troi = $gamerFactory->createTestAccount('troi');
        $generalManager->flush();

        $odinSoul = $soulFactory->create($troi, 'Odin',EnumAlignmentType::LAWFUL_EVIL);
        $odinSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVIL_BEST));
        $odinSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVERYBODY_ONLY_FRIENDS));
        $odinSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVIL_LIFE_NOT_MATTER));
        $odinSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVIL_ODINIST));

        $zeusSoul = $soulFactory->create($troi, 'Zeus',EnumAlignmentType::LAWFUL_NEUTRAL);
        $zeusSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVIL_BEST));
        $zeusSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::EVERYBODY_ONLY_FRIENDS));
        $zeusSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::MORAL_NEUTRAL_SIZE_EQUILIBRIUM));
        $zeusSoul->addPreference(new GlobalEntity\rpg\SoulPreference(GlobalEntity\rpg\SoulPreferenceTypeEnum::MORAL_NEUTRAL_POWER_EQUILIBRIUM));

        $generalManager->flush();
	}

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            PersonalFixture::class,
        ];
    }

}