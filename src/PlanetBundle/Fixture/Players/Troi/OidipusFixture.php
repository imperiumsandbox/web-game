<?php
namespace PlanetBundle\Fixture\Players\Troi;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use PlanetBundle\Entity as PlanetEntity;
use AppBundle\Entity as GlobalEntity;
use PlanetBundle\Factory\HumanAggregatorFactory;
use PlanetBundle\Factory\HumanFactory;
use PlanetBundle\Fixture\Players\TroiFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * TODO: nahradit jmeno ucelem co to ma testovat
 */
class OidipusFixture extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    /**
     * The dependency injection container.
     *
     * @var ContainerInterface
     */
    protected $container;

	public function load(ObjectManager $generalManager)
	{
        echo __CLASS__."\n";
        $soul = $generalManager->getRepository(GlobalEntity\Soul::class)->findOneBy(['name' => 'Zeus']);
        $planet = $generalManager->getRepository(GlobalEntity\SolarSystem\Planet::class)->findOneBy(['type' => GlobalEntity\SolarSystem\Planet::TEST_PLANET_SMALL]);
        $globalHumanFactory = new \AppBundle\Factory\HumanFactory($generalManager);

        $this->container->get('dynamic_planet_connector')->setPlanet($planet, true);
        $manager = $this->container->get('doctrine')->getManager('planet');

        $localHumanFactory = new HumanFactory($manager);
        $humanAggregatorFactory = new HumanAggregatorFactory($globalHumanFactory, $localHumanFactory);

        $human = $humanAggregatorFactory->create($planet, 'Oidipus');
        $human->setSoul($soul);

        $generalManager->flush();
        $manager->flush();
	}

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            TroiFixture::class,
        ];
    }

}