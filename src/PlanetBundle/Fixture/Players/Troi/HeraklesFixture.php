<?php
namespace PlanetBundle\Fixture\Players\Troi;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use PlanetBundle\Entity as PlanetEntity;
use AppBundle\Entity as GlobalEntity;
use PlanetBundle\Facade\SettlementFacade;
use PlanetBundle\Factory\HumanAggregatorFactory;
use PlanetBundle\Factory\HumanFactory;
use PlanetBundle\Factory\SettlementFactory;
use PlanetBundle\Factory\TitleFactory;
use PlanetBundle\Fixture\Players\TroiFixture;
use PlanetBundle\Fixture\StandardColonizationShipFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tracy\Debugger;

/**
 * TODO: nahradit jmeno ucelem co to ma testovat
 */
class HeraklesFixture extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    const COLONIZATION_DEPOSIT_CODE = StandardColonizationShipFixture::DEPOSIT_CODE;
    const PLANET_CODE = GlobalEntity\SolarSystem\Planet::TEST_PLANET_SMALL;
    const COLONY_POSITION = ['fst' => [0,0], 'snd' => [2,2]];
    const SOUL_NAME = 'Odin';

    /**
     * The dependency injection container.
     *
     * @var ContainerInterface
     */
    protected $container;

	public function load(ObjectManager $generalManager)
	{
        echo __CLASS__."\n";
        $soul = $generalManager->getRepository(GlobalEntity\Soul::class)->findOneBy(['name' => self::SOUL_NAME]);

        if (!$soul) {
            throw new \InvalidArgumentException("Missing soul named ".self::SOUL_NAME);
        }

        $planet = $generalManager->getRepository(GlobalEntity\SolarSystem\Planet::class)->findOneBy(['type' => self::PLANET_CODE]);
        $globalHumanFactory = new \AppBundle\Factory\HumanFactory($generalManager);

        $this->container->get('dynamic_planet_connector')->setPlanet($planet, true);
        $manager = $this->container->get('doctrine')->getManager('planet');

        $localHumanFactory = new HumanFactory($manager);
        $humanAggregatorFactory = new HumanAggregatorFactory($globalHumanFactory, $localHumanFactory);
        $settlementFactory = new SettlementFactory($manager);
        $titleFactory = new TitleFactory($generalManager, $manager, $manager->getRepository(PlanetEntity\Title::class));
        $settlementFacade = new SettlementFacade(
            $manager,
            $settlementFactory
        );

        $human = $humanAggregatorFactory->create($planet, 'Herakles');
        $human->setSoul($soul);

        $settlement = $settlementFacade->createSettement($planet, $human->getGlobalHuman(), self::COLONY_POSITION['fst'][0], self::COLONY_POSITION['fst'][1], self::COLONIZATION_DEPOSIT_CODE);
        $human->getLocalHuman()->addTitle($titleFactory->create($settlement));

        $settlement = $settlementFacade->createSettement($planet, $human->getGlobalHuman(), self::COLONY_POSITION['snd'][0], self::COLONY_POSITION['snd'][1], self::COLONIZATION_DEPOSIT_CODE);
        $human->getLocalHuman()->addTitle($titleFactory->create($settlement));
        $human->getLocalHuman()->setCurrentPeakPosition($settlement->getAdministrativeCenter());

        $manager->flush();
        $generalManager->flush();
	}

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            TroiFixture::class,
        ];
    }

}