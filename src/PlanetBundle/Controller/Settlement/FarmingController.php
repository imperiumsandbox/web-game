<?php

namespace PlanetBundle\Controller\Settlement;

use AppBundle\Builder\PlanetBuilder;
use AppBundle\Entity\Human;
use AppBundle\Entity\Human\Event;
use AppBundle\Entity\Human\EventDataTypeEnum;
use AppBundle\Entity\Human\EventTypeEnum;
use PlanetBundle\Controller\BasePlanetController;
use PlanetBundle\Entity\SettlementTitle;
use AppBundle\Entity\SolarSystem\Planet;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use PlanetBundle\Builder\BlueprintRecipe\ResourceDescriptorBuilder;
use PlanetBundle\Concept\Food;
use PlanetBundle\Concept\House;
use PlanetBundle\Concept\People;
use PlanetBundle\Entity;
use AppBundle\Repository\JobRepository;
use PlanetBundle\Entity\Peak;
use PlanetBundle\Entity\Region;
use PlanetBundle\Form\BuildersFormType;
use PlanetBundle\Form\PeakSelectorType;
use PlanetBundle\Form\RegionSelectorType;
use PlanetBundle\Repository\RegionRepository;
use PlanetBundle\View\DemographyDepositView;
use PlanetBundle\View\FarmingDepositView;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use PlanetBundle\UseCase;
use Tracy\Debugger;

/**
 * @Route(path="planet-{planet}/farming-{settlement}")
 */
class FarmingController extends BasePlanetController
{
    public function init()
    {
        parent::init();

        $settlementId = $this->get('request_stack')->getCurrentRequest()->get('settlement');
        if ($settlementId != null) {
            $settlement = $this->get('repo_settlement')->find($settlementId);
        } else {
            $settlement = $this->human->getCurrentPeakPosition()->getSettlement();
        }

        if ($settlement === null) {
            throw new NotFoundHttpException("There is no such settlement with id " . $settlementId);
        }

        $this->get('twig')->addGlobal('currentSettlement', $settlement);
        $this->get('twig')->addGlobal('currentSettlementOwner', $this->get('repo_human')->getAggregator($settlement->getOwner()));
    }


    /**
	 * @Route("/map", name="settlement_farming")
	 */
	public function mapAction(Planet $planet, Entity\Settlement $settlement, Request $request)
	{
	    $regions = [];
        /** @var Entity\Region $region */
        foreach ($this->getDoctrine()->getRepository(Region::class, 'planet')->findAll() as $region) {
            $regions[$region->getCoords()] = [
                'region' => $region,
            ];
        }

        foreach ($settlement->getRegions() as $region) {
            $regionItems = [];
            $regionProperties = [];
            $availableRecipes = [];

            $regions[$region->getCoords()]['view'] = new FarmingDepositView($region->getDeposit());
            $regions[$region->getCoords()]['recipes'] = $availableRecipes;
        }

        return $this->render('Settlement/farming.html.twig', [
            'settlement' => $settlement,
            'regions' => $regions,
            'demographyView' => new FarmingDepositView($settlement->getDeposit()),
        ]);
	}

}
