<?php

namespace PlanetBundle\Controller\Settlement;

use AppBundle\Builder\PlanetBuilder;
use AppBundle\Entity\Human;
use AppBundle\Entity\Human\Event;
use AppBundle\Entity\Human\EventDataTypeEnum;
use AppBundle\Entity\Human\EventTypeEnum;
use PlanetBundle\Controller\BasePlanetController;
use PlanetBundle\Entity\SettlementTitle;
use AppBundle\Entity\SolarSystem\Planet;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use PlanetBundle\Builder\BlueprintRecipe\ResourceDescriptorBuilder;
use PlanetBundle\Concept\Food;
use PlanetBundle\Concept\House;
use PlanetBundle\Concept\People;
use PlanetBundle\Entity;
use AppBundle\Repository\JobRepository;
use PlanetBundle\Entity\Peak;
use PlanetBundle\Entity\Region;
use PlanetBundle\Form\BuildersFormType;
use PlanetBundle\Form\PeakSelectorType;
use PlanetBundle\Form\RegionSelectorType;
use PlanetBundle\Repository\RegionRepository;
use PlanetBundle\View\DemographyDepositView;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use PlanetBundle\UseCase;
use Tracy\Debugger;

/**
 * @Route(path="planet-{planet}/demography-{settlement}")
 */
class DemographyController extends BasePlanetController
{
    public function init()
    {
        parent::init();

        $settlementId = $this->get('request_stack')->getCurrentRequest()->get('settlement');
        if ($settlementId != null) {
            $settlement = $this->get('repo_settlement')->find($settlementId);
        } else {
            $settlement = $this->human->getCurrentPeakPosition()->getSettlement();
        }

        if ($settlement === null) {
            throw new NotFoundHttpException("There is no such settlement with id " . $settlementId);
        }

        $this->get('twig')->addGlobal('currentSettlement', $settlement);
        $this->get('twig')->addGlobal('currentSettlementOwner', $this->get('repo_human')->getAggregator($settlement->getOwner()));
    }


    /**
	 * @Route("/map", name="settlement_demo")
	 */
	public function mapAction(Planet $planet, Entity\Settlement $settlement, Request $request)
	{
	    $regions = [];
        /** @var Entity\Region $region */
        foreach ($this->getDoctrine()->getRepository(Region::class, 'planet')->findAll() as $region) {
            $regions[$region->getCoords()] = [
                'region' => $region,
            ];
        }

        foreach ($settlement->getRegions() as $region) {
            $regionItems = [];
            $regionProperties = [];
            $availableRecipes = [];

            $regions[$region->getCoords()]['view'] = new DemographyDepositView($region->getDeposit(), $this->get('maintainer_population'));
            $regions[$region->getCoords()]['recipes'] = $availableRecipes;
        }

        return $this->render('Settlement/demography.html.twig', [
            'settlement' => $settlement,
            'regions' => $regions,
            'demographyView' => new DemographyDepositView($settlement->getDeposit(), $this->get('maintainer_population')),
        ]);
	}

    /**
     * @Route("/housing", name="settlement_demo_housing")
     */
    public function housingAction(Entity\Settlement $settlement, Request $request)
    {
        $houses = $settlement->getDeposit()->filterByConcept(House::class);
        $peopleBirths = 0;
        $peoples = $settlement->getDeposit()->filterByConcept(People::class);
        foreach ($this->get('maintainer_population')->getBirths($settlement->getDeposit()) as $birthCount) {
            $peopleBirths += $birthCount;
        }

        $foods = $settlement->getDeposit()->filterByUseCase(UseCase\Consumable::class);
        $foodEnergy = Entity\Deposit::sumCallbacks($foods, function ($food) { return $food->getEnergy(); });
        $housingCapacity = Entity\Deposit::sumCallbacks($houses, function (House $house) { return $house->getPeopleCapacity(); });
        $consumation = Entity\Deposit::sumCallbacks($peoples, function (People $people) { return $people->getBasalMetabolism(DynamicPlanetConnector::getPlanet()); });

        $timeLeft = Entity\Deposit::sumCallbacks($foods, function (Food $food) use ($settlement) { return $food->getTimeDeposit($settlement); });

        return $this->render('Settlement/housing.html.twig', [
            'settlement' => $settlement,
            'people' => Entity\Deposit::sumAmounts($peoples),
            'peopleBirths' => $peopleBirths,
            'foods' => $foods,
            'foodEnergy' => $foodEnergy,
            'foodVariety' => Entity\Deposit::countVariety($foods),
            'foodEnergyConsumation' => $consumation,
            'foodTimeElapsed' => $timeLeft,
            'housingCapacity' => $housingCapacity,
            'houses' => $houses,
            'human' => $this->getHuman(),
        ]);
    }


    /**
     * popup
     * @Route("/available-buildings", name="settlement_demo_build_availability")
     */
    public function availableBuildingsAction(Planet $planet, Entity\Settlement $settlement, Request $request)
    {
        $recipes = $this->getDoctrine()->getManager('planet')->getRepository(Entity\Resource\BlueprintRecipe::class)->findAll();

        return $this->render('Region/available-buildings-fragment.html.twig', [
            'builderForm' => $this->createForm(BuildersFormType::class, [], [
                'blueprints' => $recipes,
                'action' => $this->generateUrl('settlement_buildform_handler', [
                    'planet' => $this->planet->getId(),
                    'settlement' => $settlement->getId(),
                ]),
            ])->createView(),
            'settlement' => $settlement,
            'human' => $this->getHuman(),
        ]);
    }

    /**
     * @Route("/builder-form-handler", name="settlement_demo_buildform_handler")
     */
    public function handleBuilderFormAction(Planet $planet, Entity\Settlement $settlement, Request $request)
    {
        $recipe = $this->getDoctrine()->getManager('planet')->getRepository(Entity\Resource\BlueprintRecipe::class)->findAll();

        $built = 0;
        foreach ($request->get('builders_form') as $recipeId => $options) {
            if ($settlement->getDeposit() != null && isset($options['count']) && ($count = $options['count']) > 0) {
                $recipe = $this->getDoctrine()->getManager('planet')->find(Entity\Resource\BlueprintRecipe::class, $recipeId);

                // TODO: zkontrolovat, ze ma pravo stavet v tomto regionu
                $this->getDoctrine()->getManager('planet')->transactional(function ($em) use ($recipe, $settlement, $count, &$built) {
                    $builder = new ResourceDescriptorBuilder($settlement->getDeposit(), $recipe);
                    $builder->setSupervisor($this->getHuman()->getLocalHuman());
                    $builder->setAllRegionTeams();
                    $builder->setCount($count);
                    $built += $builder->build();

                    $em->persist($settlement);
                });
            }
        }

        return $this->redirectToRoute('settlement_dashboard', [
            'planet' => $this->planet->getId(),
            'settlement' => $settlement->getId(),
        ]);
    }

}
