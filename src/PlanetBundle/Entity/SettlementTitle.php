<?php
namespace PlanetBundle\Entity;

use AppBundle\Entity\Human;
use AppBundle\Entity\SolarSystem\Planet;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="human_land_titles")
 * @ORM\Entity(repositoryClass="PlanetBundle\Repository\TitleRepository")
 */
class SettlementTitle extends Title
{
    /**
     * @var Settlement
     *
     * @ORM\OneToOne(targetEntity="PlanetBundle\Entity\Settlement", inversedBy="ownerTitle")
     * @ORM\JoinColumn(name="settlement_id", referencedColumnName="id", nullable=true)
     */
    private $settlement;

    /**
     * @return Settlement
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * @param Settlement $settlement
     */
    public function setSettlement(Settlement $settlement)
    {
        $this->settlement = $settlement;
    }
}