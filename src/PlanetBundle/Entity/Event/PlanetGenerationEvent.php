<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Human;
use PlanetBundle\Entity\Settlement;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ ORM\Table(name="planet_generation_events")
 * @ORM\Entity()
 */
class PlanetGenerationEvent extends PlanetEvent
{
}

