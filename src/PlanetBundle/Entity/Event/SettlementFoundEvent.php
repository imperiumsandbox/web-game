<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity as PlanetEntity;

/**
 * @ORM\Table(name="settlement_event_foundations")
 * @ORM\Entity()
 */
class SettlementFoundEvent extends SettlementEvent
{
    /**
     * @var PlanetEntity\Human
     *
     * @ORM\ManyToOne(targetEntity="PlanetBundle\Entity\Human")
     * @ORM\JoinColumn(name="human_founder_id", referencedColumnName="id", nullable=true)
     */
    private $founder;

    /**
     * SettlementFoundEvent constructor.
     * @param PlanetEntity\Human $founder
     * @param PlanetEntity\Settlement $settlement
     * @param $phase
     */
    public function __construct(PlanetEntity\Human $founder, PlanetEntity\Settlement $settlement, $phase = null)
    {
        parent::__construct($settlement, $phase);
        $this->founder = $founder;
    }

    /**
     * @return PlanetEntity\Human
     */
    public function getFounder()
    {
        return $this->founder;
    }

}

