<?php

namespace PlanetBundle\Entity\Event;

use AppBundle\Descriptor\TimeTransformator;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Human;
use PlanetBundle\Entity\Settlement;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ ORM\Table(name="events")
 * @ ORM\Entity()
 * @ORM\MappedSuperclass()
 */
abstract class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="time", type="integer", nullable=false)
     */
    private $time;

    /**
     * @var integer
     *
     * @ORM\Column(name="planet_phase", type="integer", nullable=true)
     */
    private $planetPhase;

    public function __construct($planetPhase = null)
    {
        $this->time = time();
        if (!$planetPhase) {
            $this->planetPhase = $planetPhase;
        } else {
            $this->planetPhase = TimeTransformator::timestampToPhase(DynamicPlanetConnector::getPlanet(), time());
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return int
     */
    public function getPlanetPhase()
    {
        return $this->planetPhase;
    }

}

