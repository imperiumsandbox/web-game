<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Human;
use PlanetBundle\Entity\Settlement;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ORM\Table(name="settlement_events")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="event_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "foundation" = "SettlementFoundEvent",
 *     "building" = "SettlementBuildEvent",
 *     "expansion" = "SettlementExpandedEvent"
 *     })
 */
abstract class SettlementEvent extends Event
{
    use SettlementDependencyTrait;

    /**
     * SettlementEvent constructor.
     * @param Settlement $settlement
     * @param $phase
     */
    public function __construct(Settlement $settlement, $phase)
    {
        parent::__construct($phase);
        $this->setSettlement($settlement);
    }

}

