<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Human;
use PlanetBundle\Entity\Settlement;
use PlanetBundle\Entity\SettlementDependencyTrait;

/**
 * @ORM\Table(name="planet_events")
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="event_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "generation" = "PlanetGenerationEvent",
 *     "phase_update_starts" = "PlanetPhaseUpdateStartedEvent",
 *     "phase_update_ends" = "PlanetPhaseUpdateEndedEvent"
 *     })
 */
abstract class PlanetEvent extends Event
{
}

