<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity as PlanetEntity;
use PlanetBundle\Entity\Region;

/**
 * @ORM\Table(name="settlement_event_builds")
 * @ORM\Entity()
 */
class SettlementBuildEvent extends SettlementEvent
{
    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="PlanetBundle\Entity\Region")
     * @ORM\JoinColumns(
     *  @ORM\JoinColumn(name="region_peak_center_id", referencedColumnName="peak_center_id", nullable=false),
     *  @ORM\JoinColumn(name="region_peak_left_id", referencedColumnName="peak_left_id", nullable=false),
     *  @ORM\JoinColumn(name="region_peak_right_id", referencedColumnName="peak_right_id", nullable=false)
     * )
     */
    private $region;

    /**
     * @var PlanetEntity\Resource\Thing
     *
     * @ORM\ManyToOne(targetEntity="PlanetBundle\Entity\Resource\Thing", cascade={})
     * @ORM\JoinColumn(name="built_thing_id", referencedColumnName="id", nullable=false)
     */
    private $thing;

    /**
     * SettlementBuildEvent constructor.
     * @param Region $region
     * @param PlanetEntity\Resource\Thing $thing
     * @param PlanetEntity\Settlement $settlement
     * @param $phase
     */
    public function __construct(Region $region, PlanetEntity\Resource\Thing $thing, PlanetEntity\Settlement $settlement, $phase = null)
    {
        parent::__construct($settlement, $phase);
        $this->region = $region;
        $this->thing = $thing;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return PlanetEntity\Resource\Thing
     */
    public function getThing()
    {
        return $this->thing;
    }
}

