<?php

namespace PlanetBundle\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity as PlanetEntity;
use PlanetBundle\Entity\Region;

/**
 * @ORM\Table(name="settlement_event_expansions")
 * @ORM\Entity()
 */
class SettlementExpandedEvent extends SettlementEvent
{
    /**
     * @var Region
     *
     * @ORM\ManyToOne(targetEntity="PlanetBundle\Entity\Region")
     * @ORM\JoinColumns(
     *  @ORM\JoinColumn(name="region_peak_center_id", referencedColumnName="peak_center_id", nullable=false),
     *  @ORM\JoinColumn(name="region_peak_left_id", referencedColumnName="peak_left_id", nullable=false),
     *  @ORM\JoinColumn(name="region_peak_right_id", referencedColumnName="peak_right_id", nullable=false)
     * )
     */
    private $region;

    /**
     * SettlementExpandedEvent constructor.
     * @param Region $region
     * @param PlanetEntity\Settlement $settlement
     * @param $phase
     */
    public function __construct(Region $region, PlanetEntity\Settlement $settlement, $phase = null)
    {
        parent::__construct($settlement, $phase);
        $this->region = $region;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

}

