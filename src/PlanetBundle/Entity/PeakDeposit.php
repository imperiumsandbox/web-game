<?php

namespace PlanetBundle\Entity;

use AppBundle\Descriptor\ResourcefullInterface;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Resource\ResourceDescriptor;

/**
 * PeakResourceDeposit
 *
 * @ORM\Table(name="peak_resource_deposits")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PeakResourceDepositRepository")
 */
class PeakDeposit extends Deposit
{
    use PeakDependencyTrait;

    /**
     * PeakDeposit constructor.
     * @param Peak $peak
     * @param ResourceDescriptor[] $resourceDescriptors
     */
    public function __construct(Peak $peak, array $resourceDescriptors = [])
    {
        parent::__construct($resourceDescriptors);
        $this->setPeak($peak);
    }


    /**
     * @return ResourcefullInterface
     */
    public function getResourceHandler()
    {
        return $this->getPeak();
    }
}

