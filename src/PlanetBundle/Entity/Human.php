<?php

namespace PlanetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Human
 *
 * @ORM\Table(name="humans")
 * @ORM\Entity(repositoryClass="PlanetBundle\Repository\HumanRepository")
 */
class Human
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="global_human_code", type="string", length=32, nullable=false, unique=true)
     */
    private $globalHumanCode;

    /**
     * @var Peak
     *
     * @ORM\ManyToOne(targetEntity="\PlanetBundle\Entity\Peak")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="current_peak_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $currentPeakPosition;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="PlanetBundle\Entity\Title", mappedBy="humanHolder", cascade={"persist"})
     */
    private $titles;

    /**
     * Human constructor.
     * @param int $id
     * @param string $globalHumanCode
     * @param Peak $currentPeakPosition
     */
    public function __construct($globalHumanCode, Peak $currentPeakPosition = null, $id = null)
    {
        $this->id = $id;
        $this->globalHumanCode = $globalHumanCode;
        $this->currentPeakPosition = $currentPeakPosition;
        $this->titles = new ArrayCollection();
    }


    /**
     * @ORM\Get("id")
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGlobalHumanId()
    {
        return $this->globalHumanCode;
    }

    /**
     * @return Peak
     */
    public function getCurrentPeakPosition()
    {
        return $this->currentPeakPosition;
    }

    /**
     * @param Peak $currentPeakPosition
     */
    public function setCurrentPeakPosition(Peak $currentPeakPosition)
    {
        $this->currentPeakPosition = $currentPeakPosition;
    }

    /**
     * @return ArrayCollection
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * @param ArrayCollection $titles
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;
    }

    public function addTitle(Title $title)
    {
        $title->setHumanHolder($this);
        $this->titles->add($title);
    }

    public function __toString()
    {
        return __CLASS__."[global:".$this->getGlobalHumanId().";local:".$this->getId()."]";
    }


}

