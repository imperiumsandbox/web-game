<?php

namespace PlanetBundle\Entity;

use AppBundle\Descriptor\ResourcefullInterface;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Resource\ResourceDescriptor;

/**
 * RegionResourceDeposit
 *
 * @ORM\Table(name="region_resource_deposits")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegionResourceDepositRepository")
 */
class RegionDeposit extends Deposit
{
    use RegionDependencyTrait;

    /**
     * RegionDeposit constructor.
     * @param Region $region
     * @param ResourceDescriptor[] $resourceDescriptors
     */
    public function __construct(Region $region, array $resourceDescriptors = [])
    {
        parent::__construct($resourceDescriptors);
        $this->setRegion($region);
    }


    /**
     * @return ResourcefullInterface
     */
    public function getResourceHandler()
    {
        return $this->getRegion();
    }
}

