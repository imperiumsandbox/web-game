<?php
namespace PlanetBundle\Entity\Resource;

class MapRegionPropertyDTO
{
    /** @var string */
    private $name;
    /** @var string */
    private $value;

    /**
     * MapRegionPropertyDTO constructor.
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}