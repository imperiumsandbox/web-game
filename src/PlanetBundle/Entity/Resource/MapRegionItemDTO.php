<?php
namespace PlanetBundle\Entity\Resource;

class MapRegionItemDTO
{
    /** @var string */
    private $conceptName;
    /** @var int */
    private $count;

    /**
     * MapRegionItemDTO constructor.
     * @param string $conceptName
     * @param int $count
     */
    public function __construct(string $conceptName, int $count)
    {
        $this->conceptName = $conceptName;
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getConceptName(): string
    {
        return $this->conceptName;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

}