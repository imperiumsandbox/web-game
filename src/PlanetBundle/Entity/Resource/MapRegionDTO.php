<?php
namespace PlanetBundle\Entity\Resource;

use PlanetBundle\Concept\Concept;
use PlanetBundle\Entity\Region;

class MapRegionDTO
{
    /** @var MapRegionItemDTO[] */
    private $items;
    /** @var MapRegionPropertyDTO[] */
    private $properties;
    /** @var BlueprintRecipe[] */
    private $availableRecipes;

    /**
     * MapRegionDTO constructor.
     * @param MapRegionItemDTO[] $items
     * @param MapRegionPropertyDTO[] $properties
     * @param BlueprintRecipe[] $availableRecipes
     */
    public function __construct(array $items, array $properties, array $availableRecipes)
    {
        $this->items = $items;
        $this->properties = $properties;
        $this->availableRecipes = $availableRecipes;
    }

    /**
     * @return MapRegionItemDTO[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return MapRegionPropertyDTO[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @return BlueprintRecipe[]
     */
    public function getAvailableRecipes(): array
    {
        return $this->availableRecipes;
    }

}