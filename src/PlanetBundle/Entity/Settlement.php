<?php

namespace PlanetBundle\Entity;

use AppBundle\Descriptor\ResourcefullInterface;
use AppBundle\UuidSerializer\UuidName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PlanetBundle\Entity\Resource\Blueprint;
use PlanetBundle\Entity\Resource\DepositInterface;
use PlanetBundle\Entity\Resource\ResourceDescriptor;
use PlanetBundle\Entity\Resource\SettlementDepositsAggregator;

/**
 * Settlement - management unit
 *
 * @ORM\Table(name="settlements")
 * @ORM\Entity(repositoryClass="PlanetBundle\Repository\SettlementRepository")
 */
class Settlement implements ResourcefullInterface
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=255)
	 */
	private $type;

	/**
	 * @var Region[]
	 *
	 * @ORM\OneToMany(targetEntity="Region", mappedBy="settlement", cascade={"persist"})
	 */
	private $regions;

    /**
     * @var Peak[]
     *
     * @ORM\OneToMany(targetEntity="PlanetBundle\Entity\Peak", mappedBy="settlement", cascade={"persist"})
     */
    private $peaks;

    /**
     * @var Peak
     *
     * @ORM\OneToOne(targetEntity="PlanetBundle\Entity\Peak")
     * @ORM\JoinColumn(name="administrative_peak_id", referencedColumnName="id", nullable=false)
     */
    private $administrativeCenter;

    /**
     * @var Peak
     *
     * @ORM\OneToOne(targetEntity="PlanetBundle\Entity\Peak")
     * @ORM\JoinColumn(name="trade_peak_id", referencedColumnName="id", nullable=true)
     */
    private $tradeCenter;

    /**
     * @var SettlementTitle
     *
     * @ORM\OneToOne(targetEntity="PlanetBundle\Entity\SettlementTitle", mappedBy="settlement", cascade={"persist"})
     */
    private $ownerTitle;

    private $regionPeakIds = [];

    /**
     * Settlement constructor.
     * @param Peak $administrativeCenter
     */
    public function __construct(Peak $administrativeCenter)
    {
        $this->setAdministrativeCenter($administrativeCenter);
        $this->regions = new ArrayCollection();
        $this->peaks = new ArrayCollection();
    }

    /**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getName() {
	    return UuidName::getSettlementName($this). ' settlement';
    }

	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return Settlement
	 */
	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	// TODO: predelat tak, aby byl hlavni region nastavitelny

    /**
     * @return Region
     */
	public function getMainRegion() {
        foreach ($this->getRegions() as $region) {
            return $region;
        }
    }

	/**
	 * @return Region[]
	 */
	public function getRegions()
	{
		return $this->regions;
	}

	/**
	 * @param Region[] $regions
	 */
	public function setRegions($regions)
	{
		$this->regions = $regions;
        foreach ($regions as $region) {
            $region->setSettlement($this);
        }
	}

    /**
     * @return Peak[]
     */
    public function getPeaks()
    {
        return $this->peaks;
    }

    /**
     * @param Peak[] $peaks
     */
    public function setPeaks($peaks)
    {
        $this->peaks = $peaks;
    }

    /**
     * @return Peak
     */
    public function getAdministrativeCenter()
    {
        return $this->administrativeCenter;
    }

    /**
     * @param Peak $administrativeCenter
     */
    public function setAdministrativeCenter(Peak $administrativeCenter)
    {
        if ($administrativeCenter == null) {
            throw new \InvalidArgumentException("Administrative center cant be null");
        }
        $this->administrativeCenter = $administrativeCenter;
    }

    /**
     * @return Peak
     */
    public function getTradeCenter()
    {
        return $this->tradeCenter;
    }

    /**
     * @param Peak $tradeCenter
     */
    public function setTradeCenter(Peak $tradeCenter)
    {
        $this->tradeCenter = $tradeCenter;
    }

    /**
     * @return Human|null
     */
    public function getOwner() {
        if ($this->getOwnerTitle() != null) {
            return $this->getOwnerTitle()->getHumanHolder();
        }
        return null;
    }

    /**
     * @return SettlementTitle
     */
    public function getOwnerTitle()
    {
        return $this->ownerTitle;
    }

    /**
     * @param SettlementTitle $ownerTitle
     */
    public function setOwnerTitle(SettlementTitle $ownerTitle)
    {
        $this->ownerTitle = $ownerTitle;
    }


	/**
	 * @param $resourceDescriptor
	 * @return ResourceDescriptor[] region_coords => ResourceDescriptor[]
	 */
	public function getResources($resourceDescriptor = null)
	{
	    return $this->getDeposit()->getResourceDescriptors();
	    $resources = [];
	    /** @var Region $region */
        foreach ($this->getRegions() as $region) {
            if ($resourceDescriptor != null) {
                if (($localDeposit = $region->getResourceDeposit($resourceDescriptor)) != null) {
                    $resources[] = $localDeposit;
                }
            } else {
                foreach ($region->getResources() as $deposit) {
                    $resources[] = $deposit;
                }
            }
        }
        /** @var Peak $peak */
        foreach ($this->getPeaks() as $peak) {
            if ($resourceDescriptor != null) {
                if (($localDeposit = $peak->getResourceDeposit($resourceDescriptor)) != null) {
                    $resources[] = $localDeposit;
                }
            } else {
                foreach ($peak->getDeposit() as $deposit) {
                    $resources[] = $deposit;
                }
            }
        }
		return $resources;
	}

    /**
     * @return DepositInterface
     */
    public function getDeposit()
    {
        return new SettlementDepositsAggregator($this);
    }

    /**
     * @return Deposit[]
     */
    public function getDeposits() {
        return $this->getDeposit()->getSubDeposits();
    }

    /**
     * TODO: opravit a optimalizovat
     * @param Region $region
     * @return bool
     */
    public function isNeighbour(Region $region) : bool
    {
        return (bool) ($region->getPeakCenter()->getId() % 2);
    }

    /**
     * @return int
     */
	public function getPeopleCount() {
	    $counter = 0;
	    /** @var Region $region */
        foreach ($this->getRegions() as $region) {
            $counter += $region->getPeopleCount();
        }
        return $counter;
    }

    /**
     * @param string $resourceDescriptor
     * @return int
     */
    public function getResourceDepositAmount($resourceDescriptor)
    {
        $count = 0;
        foreach ($this->getResources($resourceDescriptor) as $deposit) {
            $count += $deposit->getAmount();
        }
        return $count;
    }

    /**
     * @param Blueprint $blueprint
     * @param int $count
     */
    public function addResourceDeposit(Blueprint $blueprint, $count = 1)
    {
        $this->getMainRegion()->addResourceDeposit($blueprint, $count);
    }

    /**
     * @param $resourceDescriptor
     * @param int $count
     */
    public function consumeResourceDepositAmount($resourceDescriptor, $count = 1)
    {
        $this->getMainRegion()->consumeResourceDepositAmount($resourceDescriptor, $count);
    }
}

