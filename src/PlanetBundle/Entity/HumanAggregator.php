<?php
namespace PlanetBundle\Entity;

use AppBundle\Entity\rpg\HumanPreference;
use AppBundle\Entity\rpg\Knowledge;
use AppBundle\Entity\Soul;

class HumanAggregator
{
    /**
     * @var \AppBundle\Entity\Human
     */
    private $globalHuman;
    /**
     * @var Human
     */
    private $localHuman;

    /**
     * HumanView constructor.
     * @param \AppBundle\Entity\Human $globalHuman
     * @param Human $localHuman
     */
    public function __construct(\AppBundle\Entity\Human $globalHuman, Human $localHuman)
    {
        $this->globalHuman = $globalHuman;
        $this->localHuman = $localHuman;
    }

    public function __get($name)
    {
        if (method_exists($this->globalHuman, $name)) {
            return $this->globalHuman->$name();
        }
        if (method_exists($this->localHuman, $name)) {
            return $this->localHuman->$name();
        }
    }

    /**
     * @return \AppBundle\Entity\Human
     */
    public function getGlobalHuman()
    {
        return $this->globalHuman;
    }

    /**
     * @return Human
     */
    public function getLocalHuman()
    {
        return $this->localHuman;
    }

    public function getName() {
        return $this->globalHuman->getName();
    }

    public function getTitle() {
        return $this->localHuman->getTitles()->getIterator()->current();
    }

    public function getTitles() {
        return $this->localHuman->getTitles();
    }

    public function getCurrentPeakPosition() {
        return $this->localHuman->getCurrentPeakPosition();
    }

    public function getDeathTime() {
        return $this->globalHuman->getDeathTime();
    }

    public function getFeelings() {
        return $this->globalHuman->getFeelings();
    }

    public function getPlanet() {
        return $this->globalHuman->getPlanet();
    }

    public function addPreference(HumanPreference $preference)
    {
        $this->globalHuman->addPreference($preference);
    }

    public function addKnowledge(Knowledge $knowledge)
    {
        $this->globalHuman->addKnowledge($knowledge);
    }

    public function setSoul(Soul $soul)
    {
        $this->globalHuman->setSoul($soul);
    }
}