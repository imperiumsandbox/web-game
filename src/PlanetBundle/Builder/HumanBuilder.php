<?php

namespace PlanetBundle\Builder;

use AppBundle\Entity\Human;
use AppBundle\PlanetConnection\DynamicPlanetConnector;
use AppBundle\Repository\HumanRepository;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity as PlanetEntity;

class HumanBuilder
{
    /** @var ObjectManager */
    private $generalEntityManager;

    /** @var ObjectManager */
    private $planetEntityManager;

    /** @var HumanRepository */
    private $generalHumanRepository;

    /**
     * HumanBuilder constructor.
     * @param ObjectManager $generalEntityManager
     * @param ObjectManager $planetEntityManager
     * @param HumanRepository $generalHumanRepository
     */
    public function __construct(ObjectManager $generalEntityManager, ObjectManager $planetEntityManager, HumanRepository $generalHumanRepository)
    {
        $this->generalEntityManager = $generalEntityManager;
        $this->planetEntityManager = $planetEntityManager;
        $this->generalHumanRepository = $generalHumanRepository;
    }

    /**
     * @param PlanetEntity\HumanAggregator $mother
     * @param PlanetEntity\HumanAggregator|null $father
     * @return PlanetEntity\HumanAggregator
     * @throws \Exception
     */
    public function create(PlanetEntity\HumanAggregator $mother, PlanetEntity\HumanAggregator $father = null) {
        if ($mother->getGlobalHuman()->getPlanet() !== DynamicPlanetConnector::$PLANET) {
            throw new \Exception("Creating human on different planet then mother is.");
        }

        /** @var PlanetEntity\Human $planetMother */
        $planetMother = $this->planetEntityManager->getRepository(PlanetEntity\Human::class)->findOneBy([
            'globalHumanCode' => $mother->getGlobalHuman()->getCode(),
        ]);

        $child = new Human();
        $child->setName('noname');
        $child->setBornPlanet($mother->getGlobalHuman()->getPlanet());
        $child->setBornPhase($mother->getGlobalHuman()->getPlanet()->getLastPhaseUpdate());
        $child->setPlanet($mother->getGlobalHuman()->getPlanet());
        $child->setMotherHuman($mother->getGlobalHuman());

        if ($father) {
            $child->setFatherHuman($father->getGlobalHuman());
        } else {
            $child->setFatherHuman(null);
        }

        $this->generalEntityManager->persist($child);
        $this->generalEntityManager->flush();

        $planetOffspring = new PlanetEntity\Human($child->getId(), $planetMother->getCurrentPeakPosition());

        $this->planetEntityManager->persist($planetOffspring);
        $this->planetEntityManager->flush();

        return $this->generalHumanRepository->getAggregator($planetOffspring);
    }
}