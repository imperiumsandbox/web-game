<?php
namespace PlanetBundle\View;

use PlanetBundle\Concept\Food;
use PlanetBundle\Concept\House;
use PlanetBundle\Concept\People;
use PlanetBundle\Entity;
use PlanetBundle\Entity\Resource\DepositInterface;
use PlanetBundle\Maintainer\LifeMaintainer;
use PlanetBundle\Maintainer\PopulationMaintainer;
use PlanetBundle\UseCase\Consumable;

class FarmingDepositView
{
    /**
     * @var DepositInterface
     */
    private $deposit;

    /**
     * @param DepositInterface $deposit
     */
    public function __construct(?DepositInterface $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * @return int
     */
    public function getPeopleCount() {
        if (!$this->deposit) {
            return 0;
        }
        return Entity\Deposit::sumAmounts($this->deposit->filterByConcept(People::class));
    }


    /**
     * @return int
     */
    public function getFoodEnergy() {
        if (!$this->deposit) {
            return 0;
        }
        return Entity\Deposit::sumCallbacks($this->deposit->filterByConcept(Consumable::class), function (Consumable $food) { return $food->getEnergy(); });
    }

    /**
     * @return float|int
     */
    public function getFoodVariety() {
        if (!$this->deposit) {
            return 0;
        }
        return Entity\Deposit::countVariety($this->deposit->filterByConcept(Consumable::class));
    }

    public function getFoods() {
        $conceptCount = [];
        if (!$this->deposit) {
            return $conceptCount;
        }
        foreach ($this->deposit->filterByConcept(Consumable::class) as $resourceDeposit) {
            $conceptCount[$resourceDeposit->getBlueprint()->getConcept()] =+ $resourceDeposit->getAmount();
        }
        return $conceptCount;
    }

    public function getFoodEnergyConsumation() {
        return 0;
    }

    public function getRunOutTimeLeft(Entity\Settlement $settlement) {
        if (!$this->deposit) {
            return null;
        }
        $foods = $this->deposit->filterByUseCase(Consumable::class);
        return Entity\Deposit::sumCallbacks($foods, function (Food $food) use ($settlement) { return $food->getTimeDeposit($settlement); });
    }
}