<?php
namespace PlanetBundle\View;

use PlanetBundle\Concept\House;
use PlanetBundle\Concept\People;
use PlanetBundle\Entity;
use PlanetBundle\Entity\Resource\DepositInterface;
use PlanetBundle\Maintainer\LifeMaintainer;
use PlanetBundle\Maintainer\PopulationMaintainer;

class DemographyDepositView
{
    /**
     * @var DepositInterface
     */
    private $deposit;
    /**
     * @var PopulationMaintainer
     */
    private $populationMaintainer;

    /**
     * DemographyDepositView constructor.
     * @param DepositInterface $deposit
     * @param PopulationMaintainer $populationMaintainer
     */
    public function __construct(?DepositInterface $deposit, PopulationMaintainer $populationMaintainer)
    {
        $this->deposit = $deposit;
        $this->populationMaintainer = $populationMaintainer;
    }

    /**
     * @return int
     */
    public function getPeopleCount() {
        if (!$this->deposit) {
            return 0;
        }
        return Entity\Deposit::sumAmounts($this->deposit->filterByConcept(People::class));
    }

    /**
     * @return int
     */
    public function getPeopleBorns() {
        $peopleBirths = 0;
        if (!$this->deposit) {
            return $peopleBirths;
        }
        foreach ($this->populationMaintainer->getBirths($this->deposit) as $birthCount) {
            $peopleBirths += $birthCount;
        }
        return $peopleBirths;
    }

    /**
     * @return int
     */
    public function getPeopleDeaths() {
        return 0;
    }

    /**
     * @return int
     */
    public function getPeopleCountDiff() {
        return $this->getPeopleBorns() - $this->getPeopleDeaths();
    }

    /**
     * @return int
     */
    public function getHousingCapacity() {
        if (!$this->deposit) {
            return 0;
        }
        return Entity\Deposit::sumCallbacks($this->deposit->filterByConcept(House::class), function (House $house) { return $house->getPeopleCapacity(); });
    }

    public function getHouses() {
        $conceptCount = [];
        if (!$this->deposit) {
            return $conceptCount;
        }
        foreach ($this->deposit->filterByConcept(House::class) as $resourceDeposit) {
            $conceptCount[$resourceDeposit->getBlueprint()->getConcept()] =+ $resourceDeposit->getAmount();
        }
        return $conceptCount;
    }
}