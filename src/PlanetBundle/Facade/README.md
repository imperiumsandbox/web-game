Facade guide
===
- no right checks
- availability checks
- changes environment properly (consumes resources, etc.)
- transactional (starts and ends its own transaction)
- creates event log