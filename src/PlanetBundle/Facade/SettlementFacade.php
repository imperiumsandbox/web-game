<?php
namespace PlanetBundle\Facade;

use AppBundle\Entity\Human;
use AppBundle\Entity\SolarSystem\Planet;
use AppBundle\Repository\ResourceDepositRepository;
use Doctrine\Common\Persistence\ObjectManager;
use PlanetBundle\Entity\Settlement;
use PlanetBundle\Factory\SettlementFactory;
use PlanetBundle\Entity as PlanetEntity;
use PlanetBundle\Repository\PeakRepository;

class SettlementFacade
{
    /**
     * @var ObjectManager $manager
     */
    private $manager;
    /**
     * @var PeakRepository
     */
    private $peakRepository;
    /**
     * @var ResourceDepositRepository
     */
    private $depositRepository;
    /**
     * @var SettlementFactory
     */
    private $settlementFactory;

    /**
     * SettlementFacade constructor.
     * @param ObjectManager $manager
     * @param PeakRepository $peakRepository
     * @param ResourceDepositRepository $depositRepository
     * @param SettlementFactory $settlementFactory
     */
    public function __construct(ObjectManager $manager, /*PeakRepository $peakRepository, ResourceDepositRepository $depositRepository, */SettlementFactory $settlementFactory)
    {
        $this->manager = $manager;
//        $this->peakRepository = $peakRepository;
//        $this->depositRepository = $depositRepository;
        $this->peakRepository =  $manager->getRepository(PlanetEntity\Peak::class);
        $this->depositRepository = $manager->getRepository(PlanetEntity\Resource\StandardizedDeposit::class);
//        $this->peakRepository = $peakRepository;
//        $this->depositRepository = $depositRepository;
        $this->settlementFactory = $settlementFactory;
    }

    /**
     * @param Planet $planet
     * @param Human $globalHuman
     * @param $xcoord
     * @param $ycoord
     * @param $colonizationDepositCode
     * @return Settlement
     */
    public function createSettement(Planet $planet, Human $globalHuman, $xcoord, $ycoord, $colonizationDepositCode) {
        /** @var PlanetEntity\Peak $administrativeCenter */
        $administrativeCenter = $this->peakRepository->findOneBy(['xcoord' => $xcoord, 'ycoord' => $ycoord ]);

        if (!$administrativeCenter) {
            throw new \InvalidArgumentException("Missing peak of [$xcoord, $ycoord]");
        }

        /** @var PlanetEntity\Resource\StandardizedDeposit $colonizationSupplies */
        $colonizationSupplies = $this->depositRepository->findOneBy(['code' => $colonizationDepositCode]);

        if (!$colonizationSupplies) {
            throw new \InvalidArgumentException("Missing supply deposit with code $colonizationDepositCode");
        }

        /** @var PlanetEntity\Settlement $settlement */
        $settlement = $this->settlementFactory->createColony($administrativeCenter, $colonizationSupplies);
        return $settlement;
    }
}