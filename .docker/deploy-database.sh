#!/bin/sh/var/www/mysite

cd /var/www/mysite

bin/console doctrine:schema:drop --force --env=$1 --em=default
bin/console doctrine:schema:drop --force --env=$1 --em=planet_test1
bin/console doctrine:schema:drop --force --env=$1 --em=planet_test2
bin/console doctrine:schema:create --env=$1 --em=default
bin/console doctrine:schema:create --env=$1 --em=planet_test1
bin/console doctrine:schema:create --env=$1 --em=planet_test2
bin/console doctrine:fixtures:load --env=$1 --em=default -qvv
